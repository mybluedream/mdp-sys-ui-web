/* Layout */
import Layout from '../views/layout/Layout'
const _import = require('./_import_' + process.env.NODE_ENV) 
 
 
export default { 
	routes:[
		{
	        path: '/mdp/form',
	        component: Layout,
	        name: '智能表单',
	        iconCls: 'fa el-icon-menu',
		    meta: {
		       title: 'IntelligentForm',
		       icon: 'capacity-form'
		    }, 
	        //leaf: true,//只有一个节点
	        children: [
				{
					iconCls: 'fa el-icon-menu', path: 'index', component: _import("mdp/form/formDef/Index"), name: 'FormDefMng' ,meta:{
					title: 'IntelligentForm',
					icon: 'capacity-form'} },
				{ path: 'design/index/:expandId', component: _import("mdp/form/formDef/Index"), name: 'FormDefMng',meta:{title:'FormDefMng'} ,hidden:true },
				{ path: 'design/:expandId', component: _import("mdp/form/formDef/DesignRoute"), name: 'FormDesign',meta:{title:'FormDefMng'} ,hidden:true },
		
				{ path: 'data/edit/:formId/:dataId', component: _import("mdp/form/formData/FormRoute"), name: '数据填报',meta:{title:'FormDataAddRoute'} ,hidden:true },

 	        	{ path: 'data/add/:formId', component: _import("mdp/form/formData/FormRoute"), name: '数据填报',meta:{title:'FormDataAddRoute'} ,hidden:true },
 	        	{ path: 'data/index/:formId', component: _import("mdp/form/formData/Index"), name: '智能表单数据管理',meta:{title:'FormDataMng' } ,hidden:true },

	            { path: 'data/mng', component: _import("mdp/form/formData/Index"), name: 'FormDataMng', meta:{title:'FormDataMng'},hidden:true }
	        ]
	    }
	]
}
